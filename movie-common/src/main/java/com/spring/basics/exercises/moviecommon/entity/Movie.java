package com.spring.basics.exercises.moviecommon.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "movie", schema = "Public")
@Getter
@Setter
@NoArgsConstructor
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private Long movieId;

    private String title;

    private int budget;

    private LocalDate release;

    private String genre;
    @Column(name = "imdb_rating", columnDefinition = "NUMERIC(2, 1)")
    private float imdb_rating;
    @Column(name = "imdb_votes")
    private int imdb_votes;
}