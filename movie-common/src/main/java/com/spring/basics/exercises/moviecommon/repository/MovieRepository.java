package com.spring.basics.exercises.moviecommon.repository;


import com.spring.basics.exercises.moviecommon.entity.Movie;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends CrudRepository<Movie, Long> {
}
