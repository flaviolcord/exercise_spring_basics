CREATE TABLE IF NOT EXISTS movie
(
    movie_id    bigserial PRIMARY KEY,
    title       varchar NOT NULL,
    budget      integer,
    release     date    NOT NULL,
    genre       varchar,
    imdb_rating numeric(2, 1),
    imdb_votes  integer
);

