package com.spring.basics.exercises.moviebatch.dto.mapper;

import com.spring.basics.exercises.moviebatch.dto.MovieDto;
import com.spring.basics.exercises.moviebatch.dto.mapper.custom.LocalDateMapper;
import com.spring.basics.exercises.moviecommon.entity.Movie;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring",
        uses = LocalDateMapper.class)
public interface MovieMapper {
    Movie toEntity(MovieDto movieDto);
}
